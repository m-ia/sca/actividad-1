# Instrucciones de instalación de Cuda en WSL2

Como uso WSL para ejecutar este proyecto, voy a poner los pasos que he dado a grosso-modo, ya que he hecho una cantidad enorme de cosas.

Lo suyo es instalar [Cuda for WSL2](https://docs.nvidia.com/cuda/wsl-user-guide/index.html) y [cuDNN](https://developer.nvidia.com/cudnn-downloads?target_os=Linux&target_arch=x86_64&Distribution=Ubuntu&target_version=22.04&target_type=deb_local)

Hay que fijarse también en el código del proyecto y sobre todo en `requirements.txt`, puesto que es necesario el paquete 
`tensorflow[and-cuda] == 2.15.1` y no es `tensorflow == 2.16.1`.

¡¡NOTA!! A día de hoy, 2024-05-06, Tensorflow 2.16.1 tiene un bug que impide el uso de nuestras GPUs Nvidia. Por eso, hay que usar una versión anterior.

Es posible que sea necesario editar el fichero `~.bashrc`:

```
# Cuda
export PATH=/usr/local/cuda-12.4/bin${PATH:+:${PATH}}
export LD_LIBRARY_PATH=/usr/local/cuda-12.4/lib64${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}
export CUDA_PATH=/usr/local/cuda-12.4
export TF_CUDA_PATHS=/usr/local/cuda-12.4
```

Recordar que mi equipo tiene una CPU Intel Core I7-10700K con una GPU Nvidia GTX 1070.